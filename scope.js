//defined outside all code block
//defined inside in code block
//global scope (varone)
//local scope (vartwo)
//local scope (varFour)
//local scope (varthree)

let varOne = 'varOne'
if (true) {
           console.log(varOne)
           let varTwo = 'varTwo'
           console.log(varTwo)
           
           if(true) {
              let varFour = 'varFour'       
           }
        }

if(true) {  
          letvarThree('varThree')
}
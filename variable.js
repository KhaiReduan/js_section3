//you can define more variable more than once
let myName = 'Khairul'

myName = 'Izham'

console.log(myName)

//there are rules related to variable names
let name1 = 2
let name_ = 3 + 4
let full = name1 * name_
console.log(full)

